/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bintang;

/**
 *
 * @author andi
 */
import java.util.Scanner;
public class Bintang {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        main();
    }
    
    public static void main(){
        Scanner inp = new Scanner(System.in);
        int maxnoaplikasi = 11;
        System.out.println("List Aplikasi Bintang :");
        for (int i = 0; i <= maxnoaplikasi ; i++){
            switch (i){
                case 1:
                    System.out.print(" " + i+ ". Bintang Segitiga Rata Kiri" + "\t\t\t\t\t\t\t\t");
                    break;
                case 2:
                    System.out.println(" " + i+ ". Bintang Segitiga Rata Kanan");
                    break;
                case 3:
                    System.out.print(" " + i+ ". Bintang Break Satu enter" + "\t\t\t\t\t\t\t\t");
                    break;
                case 4:
                    System.out.println(" " + i+ ". Bintang Segitiga Sempurna");
                    break;
                case 5:
                    System.out.print(" " + i+ ". Bintang Segitiga Tengahnya Kosong" + "\t\t\t\t\t\t\t");
                    break;
                case 6:
                    System.out.println(" " + i+ ". Bintang Persegi Tengahnya Kosong");
                    break;
                case 7:
                    System.out.print(" " + i+ ". Bintang Trapesium Sama Kaki" + "\t\t\t\t\t\t\t\t");
                    break;
                case 8:
                    System.out.println(" " + i+ ". Bintang Bentuk plus (+)");
                    break;
                case 9:
                    System.out.print(" " + i+ ". Bintang Persegi Tengahnya Kosong dan hanya ada satu bintang" + "\t\t\t\t");
                    break;
                case 10:
                    System.out.println(i+ ". Bintang Bentuk Belah Ketupat");
                    break;
                case 11:
                    System.out.println(i+ ". Segitiga Terbalik");
                    break;
                default:
                    break;
            }
        }
        
        System.out.print("Inputkan no urut aplikasi bintang : ");
        int input = inp.nextInt();
        proses(input);
        
    }
    
    public static void proses(int param){
        switch (param){
            case 1:
                case1();
                end();
            break;
            case 2:
                case2();
                end();
            break;
            case 3:
                case3();
                end();
            break;
            case 4:
                case4();
                end();
            break;
            case 5:
                case5();
                end();
            break;
            case 6:
                case6();
                end();
            break;
            case 7:
                case7();
                end();
            break;
            case 8:
                case8();
                end();
            break;
            case 9:
                case9();
                end();
            break;
            case 10:
                case10();
                end();
            break;
            case 11:
                case11();
                end();
            break;
            default:
                System.out.println("Default");
        }
    }
    
    public static void end(){
        Scanner inp = new Scanner(System.in);
        System.out.print("Mau lanjut??...... (y/n): ");
        String type = inp.next().toLowerCase();
        System.out.println(type);
        switch (type) {
            case "y":
                for (int i = 0 ; i < 10 ; i++){
                    System.out.println();
                }                
                main();
                break;
            default:
                System.out.println("Terimakasih");
                break;
        }
        
    }
    
//    Bintang Segitiga Rata Kiri
    public static void case1(){
        Scanner inp = new Scanner(System.in);
        
        int i = 1;
        int j;
        System.out.println("SEGITIGA RATA KIRI");        
        System.out.print("input batas baris: ");
        int batas = inp.nextInt();
        while (i <= batas) {
            for (j=1;j<=i;j++) {
                System.out.print("*");
            }
            System.out.println("");
            i++;
        }        
    }
    
//    Bintang Segitiga Rata Kanan
    public static void case2(){
        Scanner inp = new Scanner(System.in);
        int i = 1;
        int j;
        System.out.println("SEGITIGA RATA KANAN");        
        System.out.print("input batas baris : ");
        int batas = inp.nextInt();
        while (i <= batas) {
            for (j=i;j<=(batas-1);j++) {
                System.out.print(" ");
            }            
            for (j=i;j>=1;j--) {
                System.out.print("*");
            }
            System.out.println("");
            i++;
        }
    }
    
//    Bintang Break Satu enter
    public static void case3(){
        Scanner inp = new Scanner(System.in);
        int i = 1;
        int j;
        System.out.println("BINTANG BERJARAK");        
        System.out.print("input batas : ");
        int batas = inp.nextInt();
        while (i <= batas) {
            for (j=1;j<=batas;j++) {
                System.out.print("*");
            }            
            
            System.out.println("");
            System.out.println("");
            i++;
        }    
    }
    
//    Bintang Segitiga Sama Sisi
    public static void case4(){
        Scanner inp = new Scanner(System.in);
        int i = 1;
        int j;
        System.out.println("Segitiga Sempurna");        
        System.out.print("input batas baris : ");
        int batas = inp.nextInt();
        while (i <= batas) {
            for (j=i;j<=(batas-1);j++) {
//                for (int l = )
                System.out.print(" ");
            }            
            for (j=i;j>=1;j--) {
                if (i != 1) {
                    System.out.print(" *");
                } else {
                    System.out.print(" *");
                }
            }
            System.out.println("");
            i++;
        } 
    }
    
//    Bintang Segitiga Tengahnya Kosong
    public static void case5(){
        Scanner inp = new Scanner(System.in);
        int i = 1;
        int j;
        System.out.println("Segitiga Segitiga Tengahnya Kosong");        
        System.out.print("input batas baris : ");
        int batas = inp.nextInt();
        while (i <= batas) {
            for (j=i;j<=(batas-1);j++) {
//                for (int l = )
                System.out.print(" ");
            }            
            for (j=i;j>=1;j--) {
                if (i != 1) {
                    System.out.print(" ");
                    if (i == batas || i == 1 || j == 1 || j == i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                } else {
                    System.out.print(" *");
                }
            }
            System.out.println("");
            i++;
        }        
    
    }
    
//    Bintang Persegi Tengahnya Kosong"
    public static void case6(){
        Scanner inp = new Scanner(System.in);
        int i = 1;
        int j;
        System.out.println("Bintang Persegi Tengahnya Kosong"); 
        System.out.println("NB: nilai kurang dari 3 default 4");
        System.out.print("input batas panjang: ");
        int panjangint = inp.nextInt();
        int panjang = panjangint < 3 ? 4 : panjangint;
        System.out.print("input batas tinggi: ");
        int tinggiint = inp.nextInt();
        int tinggi = tinggiint < 3 ? 4 : tinggiint;
        while (i <= tinggi) {
            for (j=1;j<=panjang;j++) {
                boolean valid1 = i == 1 || i == tinggi || j == 1 || j == panjang ? true : false;
                if (valid1 == true){
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
                
            }            
            System.out.println("");
            i++;
        }        
    }
    
//    Bintang Trapesium Sama Kaki
    public static void case7(){
        Scanner inp = new Scanner(System.in);
        
        int i = 1;
        int j;
        System.out.println("Bintang Trapesium Sama Kaki");        
        System.out.print("input batas baris : ");
        int batas = inp.nextInt();
        while (i <= batas) {
            for (j=i;j<=(batas-1);j++) {
                System.out.print(" ");
            }            
            for (j=i;j>=1;j--) {
                System.out.print("*");
            }
            for (j=1;j<=batas;j++) {
                System.out.print("*");
            } 
            for (j=1;j<=i;j++) {
                System.out.print("*");
            }            
            System.out.println("");
            i++;
        }
    }
    
//    Bintang Bentuk plus (+)
    public static void case8(){
        Scanner inp = new Scanner(System.in);
    int i = 1;
        int j;
        System.out.println("Bintang Bentuk plus (+)");        
        System.out.print("input batas baris : ");
        int batas = inp.nextInt();
        while (i <= batas) {
            for (j=i;j<=(batas-1);j++) {
//                for (int l = )
                System.out.print(" ");
            }            
            for (j=i;j>=1;j--) {
                if (i != 1) {
                    System.out.print(" ");
                    if (i == batas || i == 1 || j == 1) {
                        System.out.print("*");
                    }
                } else {
                    System.out.print(" *");
                }
            }
            System.out.println("");
            i++;
        }
        i = 1;
        while (i <= batas) {
            for (j=i;j<=(batas-1);j++) {
//                for (int l = )
                System.out.print(" ");
            }            
            for (j=i;j>=1;j--) {
                if (i != 1) {
                    System.out.print(" ");
                    if (i == 1 || j == 1) {
                        System.out.print("*");
                    }
                } else {
                    System.out.print(" *");
                }
            }
            System.out.println("");
            i++;
        }        
    
    }
    
//    Bintang Persegi Tengahnya Kosong dan hanya ada satu bintang
    public static void case9(){
        Scanner inp = new Scanner(System.in);
        
        int i = 1;
        int j;
        System.out.println("Bintang Persegi Tengahnya Kosong ada satu bintang"); 
        System.out.println("NB1: nilai kurang dari 3 default 4");
        System.out.println("NB1: nilai genap akan otomatis + 1");        
        System.out.print("input batas panjang: ");
        int panjangint = inp.nextInt();
        int panjang = panjangint < 3 ? 4 : ( (panjangint%2) == 0 ? panjangint+1  : panjangint);
        System.out.print("input batas tinggi: ");
        int tinggiint = inp.nextInt();
        int tinggi = tinggiint < 3 ? 4 : ( (tinggiint%2) == 0 ? tinggiint+1  : tinggiint);
        while (i <= tinggi) {
            for (j=1;j<=panjang;j++) {
                int titikTengah = (tinggi/2) + 1;
                int titikTengah2 = (panjang/2) + 1;
                boolean valid1 = i == 1 || i == tinggi || j == 1 || j == panjang || (i == titikTengah && j == titikTengah2) ? true : false;
                if (valid1 == true){
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
                
            }            
            System.out.println("");
            i++;
        }        
            
    }
    
//    Bintang Bentuk Belah Ketupat
    public static void case10(){
        Scanner inp = new Scanner(System.in);

        int i = 1;
        int j;
        System.out.println("Bintang Bentuk Belah Ketupat");        
        System.out.print("input batas baris : ");
        int batas = inp.nextInt();
        while (i <= batas) {
            for (j=i;j<=(batas-1);j++) {
//                for (int l = )
                System.out.print(" ");
            }            
            for (j=i;j>=1;j--) {
                if (i != 1) {
                    System.out.print(" *");
                } else {
                    System.out.print(" *");
                }
            }
            System.out.println("");
            i++;
        }
        
        i=1;
        while (i <= batas) {
            for (j=batas;j>=(batas-i);j--) {
//                for (int l = )
                System.out.print(" ");
            }             
            for (j=1;j<=(batas-i);j++) {
                if (i != 2) {
                    System.out.print("* ");
                } else {
                    System.out.print("* ");
                }
            }
            System.out.println("");
            i++;
        }
    }
    
//    Segitiga Terbalik
    public static void case11(){
        Scanner inp = new Scanner(System.in);
        
        int i = 1;
        int j;
        System.out.println("Segitiga Terbalik");        
        System.out.print("input batas baris : ");
        int batas = inp.nextInt();
        while (i <= batas) {
            for (j=batas;j>=(batas-i);j--) {
//                for (int l = )
                System.out.print(" ");
            }             
            for (j=1;j<=(batas-i);j++) {
                if (i != 2) {
                    System.out.print("* ");
                } else {
                    System.out.print("* ");
                }
            }
            System.out.println("");
            i++;
        }   
    }
    
}
